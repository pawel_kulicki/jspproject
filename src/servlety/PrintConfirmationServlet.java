package servlety;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Database;

/**
 * Servlet obsługujący proces drukowania potwierdzenia
 * 
 * @author kulickip
 */
@WebServlet({ "/PrintConfirmationServlet" })
public class PrintConfirmationServlet extends HttpServlet {
	public PrintConfirmationServlet() {
	    super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "/WydajKarte.jsp";
		HttpSession session = request.getSession();
		boolean print = Boolean.valueOf(request.getParameter("print"));
		
		if (print) {//drukuj potwierdzenie
			String userFullName = session.getAttribute("userFullName").toString();
			String cashToBePaid = session.getAttribute("cashToBePaid").toString();
			int cardId = Integer.valueOf(session.getAttribute("cardId").toString());
			String accountNumber = Database.selectAccountNumber(cardId);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String currentDate = sdf.format(new Date());
			
			System.out.println("////////POTWIERDZENIE////////");
			System.out.println("Numer konta: "+accountNumber);
			System.out.println("Wypłacający: "+userFullName);
			System.out.println("Kwota: "+cashToBePaid);
			System.out.println("Dnia: "+currentDate);
			System.out.println("////////POTWIERDZENIE////////");
		} 
		
		ServletContext context = getServletContext();
		RequestDispatcher dispatcher = context.getRequestDispatcher(url);
		dispatcher.forward(request, response);
		
		session.invalidate(); //wyczyść dane sesji
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
