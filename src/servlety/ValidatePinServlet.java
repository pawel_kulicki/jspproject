package servlety;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Database;

/**
 * Servlet obs�uguj�cy proces walidacji Pinu karty kredytowej.
 * 
 * @author kulickip
 */
@WebServlet({ "/ValidatePinServlet" })
public class ValidatePinServlet extends HttpServlet {
    
   public ValidatePinServlet() {
       super();
   }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "";
		String ownerLogin = request.getSession().getAttribute("userLogin").toString();		
		
		if (request.getParameter("pin").length() == 0 || request.getParameter("pin") == null) {
			url = "/PinPanel.jsp";
			request.setAttribute("error", "Wprowadz Pin.");
		} else {
			int pin = 0;
			try {
				pin = Integer.valueOf(request.getParameter("pin"));
			} catch (Exception e) {
				url = "/PinPanel.jsp";
				request.setAttribute("error", "Pin mo�e zawiera� tylko cyfry.");
				ServletContext context = getServletContext();
				RequestDispatcher dispatcher = context.getRequestDispatcher(url);
				dispatcher.forward(request, response);
				return;
			}
			
			if (Database.validatePin(ownerLogin, pin)) {
				int incorrectEnteredPinCounter = Database.getIncorrectEnteredPinCounter(ownerLogin);
				
				if (incorrectEnteredPinCounter >= 3) {
					url = "/ZatrzymajKarte.jsp";
				} else {//ok
					url = "/WprowadzKwote.jsp";
					request.getSession().setAttribute("cardId", Database.selectCardId(ownerLogin, pin));
				}
			} else {
				int incorrectEnteredPinCounter = Database.getIncorrectEnteredPinCounter(ownerLogin);
				
				if (incorrectEnteredPinCounter >= 3) {
					url = "/ZatrzymajKarte.jsp";
					request.getSession().invalidate();
				} else {
					incorrectEnteredPinCounter ++;
					Database.setIncorrectEnteredPinCounter(ownerLogin, incorrectEnteredPinCounter);
					
					url = "/PinPanel.jsp";
					request.setAttribute("error", "B��dny PIN! Pozosta�o pr�b : " + (3-incorrectEnteredPinCounter));
				}
			}
		}
		
		ServletContext context = getServletContext();
		RequestDispatcher dispatcher = context.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
