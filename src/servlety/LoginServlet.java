package servlety;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Database;

/**
 * Servlet waliduj�cy nazwe oraz login u�ytkownika
 */
@WebServlet({ "/HelloServlet", "/LoginServlet", "/Hello.html" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "";
		String user = request.getParameter("user");
		String password = request.getParameter("password");
		String fullName = "";
		
		if (user == null || user.length() == 0) {
			url = "/index.jsp";
			request.setAttribute("error", "Podane imi� nie mo�e by� puste.");
		} else {
			Database database = new Database();
			fullName = database.lookupFullname(user, password);
			
			if (fullName == null) {
				url = "/index.jsp";
				request.setAttribute("error", "Bledny Login lub Haslo.");
			} else {
				url = "/welcome.jsp";
				request.setAttribute("userFullName", fullName);
				request.setAttribute("userLogin", user);

			}
		}
		
		ServletContext context = getServletContext();
		RequestDispatcher dispatcher = context.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
