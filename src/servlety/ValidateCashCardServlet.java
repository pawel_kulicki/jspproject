package servlety;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet s�u��cy do walidacji karty bankomatowej. Dla uproszczenia przyj�to
 * �e walidacja jest zawsze zwraca true.
 * 
 * @author kulickip
 */
@WebServlet({ "/ValidateCashCardServlet"})
public class ValidateCashCardServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
     * @see HttpServlet#HttpServlet()
     */
    public ValidateCashCardServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "";
		String userFullName = request.getParameter("userFullName");
		
		if (true) {
			url = "/PinPanel.jsp";
			request.setAttribute("ok", "ok");
		} else {
			url = "/welcome.jsp";
			request.setAttribute("error", "B��dna walidacja karty, wprowadz inn� kart�!");
		}
		
		request.setAttribute("userFullName", userFullName);
		ServletContext context = getServletContext();
		RequestDispatcher dispatcher = context.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
