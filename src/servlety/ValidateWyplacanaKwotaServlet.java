package servlety;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Database;

/**
 * Servlet obs�uguj�cy proces wyp�a� got�wk�.
 * 
 * @author kulickip
 */
@WebServlet({ "/ValidateWyplacanaKwotaServlet" })
public class ValidateWyplacanaKwotaServlet extends HttpServlet {
	
	public ValidateWyplacanaKwotaServlet() {
	    super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "";
		String cashToBePaidInString = request.getParameter("cash");
		int cardId = Integer.valueOf(request.getSession().getAttribute("cardId").toString());		

		if (cashToBePaidInString.length() == 0 || cashToBePaidInString == null) {
			url = "/WprowadzKwote.jsp";
			request.setAttribute("error", "Prosze wprowadzi� kwote.");
		} else {
			int cashToBePaid = Integer.valueOf(cashToBePaidInString);
			int cashInAccount = Database.selectAccountCash(cardId);
			
			int cashInAccountAfterWithDraw = cashInAccount - cashToBePaid;
			if (cashInAccountAfterWithDraw < 0) {
				url = "/WprowadzKwote.jsp";
				request.setAttribute("error", "Masz za ma�o sierodk�w na koncie.");
			} else {
				request.getSession().setAttribute("cashToBePaid", cashToBePaid);
				url = "/WyplacGotowke.jsp";
				Database.updateCashInAccount(cardId, cashInAccountAfterWithDraw);
			}
		}
		
		ServletContext context = getServletContext();
		RequestDispatcher dispatcher = context.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
