package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Klasa obsługująca połączenie do bazy danych oraz podstawowe operacje
 * SQL na bazie danych.
 * 
 * @author kulickip
 */
public class Database extends SQLException {
	
	private static final long serialVersionUID = 1L;

	public static Connection connect() {
		Connection connection = null;

		String DRIVER = "com.mysql.jdbc.Driver";
		String URL = "jdbc:mysql://localhost:3306/testJSP";
		String USER = "root";
		String PASSWORD = "pawel1";
		
		try {
			Class.forName(DRIVER);
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getLocalizedMessage());
		} catch (SQLException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		return connection;
	}

	public String lookupFullname(String userLogin, String userPassword) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String fullname = "";

		String QUERY = "SELECT FullName FROM User WHERE Login = ? AND Password = ?";

		try {
			connection = Database.connect();
			statement = connection.prepareStatement(QUERY);
			statement.setString(1, userLogin);
			statement.setString(2, userPassword);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				fullname = resultSet.getString("FullName");
				return fullname;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(resultSet,  statement, connection);
		}
		
		return null;
	}
	
	public static boolean validatePin(String ownerLogin, int pin) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String QUERY = "SELECT cardId FROM CREDIT_CARD WHERE ownerLogin = ? AND PIN = ?";

		try {
			connection = Database.connect();
			statement = connection.prepareStatement(QUERY);
			statement.setString(1, ownerLogin);
			statement.setInt(2, pin);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(resultSet,  statement, connection);
		}
		
		return false;
	}
	
	public static void updateCashInAccount(int cardId, int cash) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String QUERY = "UPDATE ACCOUNT SET cash = ? WHERE cardId = ? ";

		try {
			connection = Database.connect();
			statement = connection.prepareStatement(QUERY);
			statement.setInt(1, cash);
			statement.setInt(2, cardId);

			statement.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(resultSet,  statement, connection);
		}
	}
	
	public static String selectAccountNumber(int cardId) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String QUERY = "SELECT accountNumber FROM ACCOUNT WHERE cardId = ? ";

		try {
			connection = Database.connect();
			statement = connection.prepareStatement(QUERY);
			statement.setInt(1, cardId);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getString("accountNumber");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(resultSet,  statement, connection);
		}
		
		return "";
	}
	
	public static int selectAccountCash(int cardId) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String QUERY = "SELECT cash FROM ACCOUNT WHERE cardId = ? ";

		try {
			connection = Database.connect();
			statement = connection.prepareStatement(QUERY);
			statement.setInt(1, cardId);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getInt("cash");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(resultSet,  statement, connection);
		}
		
		return -1;
	}
	
	public static int selectCardId(String ownerLogin, int pin) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String QUERY = "SELECT cardId FROM CREDIT_CARD WHERE ownerLogin = ? AND PIN = ?";

		try {
			connection = Database.connect();
			statement = connection.prepareStatement(QUERY);
			statement.setString(1, ownerLogin);
			statement.setInt(2, pin);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getInt("cardId");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(resultSet,  statement, connection);
		}
		
		return -1;
	}
	
	public static int getIncorrectEnteredPinCounter(String ownerLogin) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String QUERY = "SELECT incorrectEnteredPinCounter FROM CREDIT_CARD WHERE ownerLogin = ?";

		try {
			connection = Database.connect();
			statement = connection.prepareStatement(QUERY);
			statement.setString(1, ownerLogin);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				int counter = resultSet.getInt("incorrectEnteredPinCounter");
				
				return counter;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(resultSet,  statement, connection);
		}
		
		return -1;
	}
	
	public static void setIncorrectEnteredPinCounter(String ownerLogin, int incorrectEnteredPinCounter) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		String QUERY = "UPDATE CREDIT_CARD SET incorrectEnteredPinCounter = ? WHERE ownerLogin = ?";

		try {
			connection = Database.connect();
			statement = connection.prepareStatement(QUERY);
			statement.setInt(1, incorrectEnteredPinCounter);
			statement.setString(2, ownerLogin);
			statement.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(resultSet,  statement, connection);
		}
	}
	
	private static void closeConnection(ResultSet resultSet, PreparedStatement statement, Connection connection) {
		try {
			if (resultSet != null)
				resultSet.close();
			if (statement != null)
				statement.close();
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
}
