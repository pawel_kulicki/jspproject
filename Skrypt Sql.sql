
CREATE TABLE USER(
   userId INT NOT NULL AUTO_INCREMENT,
   Login VARCHAR(100) NOT NULL,
   Password VARCHAR(140) NOT NULL,
   FullName VARCHAR(140) NOT NULL,
   PRIMARY KEY ( userId ),
    UNIQUE KEY `Login_UNIQUE` (`Login`)
);
CREATE TABLE CREDIT_CARD(
   cardId INT NOT NULL AUTO_INCREMENT,
   PIN INT NOT NULL,
   ownerLogin Varchar(100) NOT NULL,
   incorrectEnteredPinCounter INT,
   PRIMARY KEY ( cardId ),
   FOREIGN KEY (ownerLogin) REFERENCES User(Login)
);

CREATE TABLE ACCOUNT(
   accountId INT NOT NULL AUTO_INCREMENT,
   accountNumber VARCHAR(100) NOT NULL,
   ownerLogin VARCHAR(100) NOT NULL,
   cardId int NOT NULL,
   cash int NOT NULL,
   PRIMARY KEY ( accountId ),
   FOREIGN KEY (ownerLogin) REFERENCES User(Login),
   FOREIGN KEY (cardId) REFERENCES CREDIT_CARD(cardId)
);

INSERT INTO `testjsp`.`user`
(`userId`,
`Login`,
`Password`,
`FullName`)
VALUES
(1,
"admin",
"pawel1",
"Paweł Kulicki");

INSERT INTO `testjsp`.`credit_card`
(`cardId`,
`PIN`,
`ownerLogin`,
`incorrectEnteredPinCounter`)
VALUES
(1,
1234,
"admin",
0);

INSERT INTO `testjsp`.`account`
(`accountId`,
`accountNumber`,
`ownerLogin`,
`cardId`,
`cash`)
VALUES
(1,
"3333 4431 0000 0000 1121 4321",
"admin",
1,
520);

