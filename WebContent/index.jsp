<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login User</title>
</head>
<body>
	<h1>Panel Logowania:</h1>
	<%
	String error_message = "";
	Object error = request.getAttribute("error");
	if (error != null)
		error_message = error.toString();
	
	%>
	<%-- <c:set var="test" value="Parameter 1" scope="session"></c:set> --%>
	<jsp:useBean id="loginBean" class="pl.imsi.LoginBean" scope="session"></jsp:useBean>
	<jsp:setProperty property="name" name="loginBean" value="${param.user}"/>
	<jsp:setProperty property="password" name="loginBean" value="${param.password}"/>
	<%-- <c:out value="${loginBean.name}"></c:out> --%>
	<%-- <c:out value="${loginBean.password}"></c:out> --%>
	<form action="LoginServlet">
		<input type="text" name="user" value="${loginBean.name}">
		<input type="password" name="password" value="${loginBean.password}">
		<input type="submit" name="user" value="Zaloguj">
	</form>
	<%-- <c:out value="${param.user}"></c:out> --%>
	<!-- <a href="Test1.jsp">kliknij</a> -->
	<p style="color: red"><%=error_message%></p>
</body>
</html>
