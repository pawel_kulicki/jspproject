<%@ page language="java" contentType="text/html; charset=UTF-8" 
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
</head>
<body>
<%

String error_message = "";
Object error = request.getAttribute("error");
if (error != null)
	error_message = error.toString();
%>
<div style="background: url(bankomat.png) repeat; width:750px;height:500px; border: 2px solid black;">
	<form action="ValidatePinServlet" style="margin-top:180px;margin-left:210px">
		<h1>Wprowadź pin</h1>
		<br>
		
		<input name="pin" type="password" size="20" autocomplete="off"/>
		<br><br>
		<button type="submit" style="height:30px; width:200px">Zatwierdz</button>
		<div>
			<p style="color: red"><%=error_message%></p>
		</div>
	</form>
</div>

</body>
</html>